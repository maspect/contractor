"""A simple api to request documents."""
from os import path, makedirs
import subprocess
from tempfile import NamedTemporaryFile

from flask import current_app, abort, make_response, jsonify
from werkzeug.utils import secure_filename
import requests

from schema import Schema, Use, And, Or, Optional, SchemaError


def parse_company_data(data):
    """Validate and format data for company.

    Check if all information is provided, and process images.
    The logo is expected as .svg, and is downloaded and converted to pdf.
    The ad is exepcted as .pfg.

    Args:
        data (dict): The company data.

    Returns:
        dict: company information, with filepaths for media.

    Raises:
        BadRequest: Data cannot be parsed as json (status code 400).
        UnprocessableEntity: Validation Error (code 422). Error message in body.

    Examples:
        This is an example of a complete dictionary including everything:

        ```
        {
            'name': "Testcorp",
            'website': 'test.corp.com',
            'contact': 'Mr. M. Anager\nm.anager@testcorp.com',
            'employees_ch': 42,
            'employees_world': 420,

            'booth': 'A32',
            'interested_in': ['itet', 'mavt', 'mtec'],
            'offers': ["bachelor", "master", "semester",
                       "fulltime", "internship", "trainee"],

            'about': 'Eine sehr aufschlussreiche Beschreibung.\nMehrzeilig!',
            'focus': 'Gefolgt von nützlichen Details.',

            'logo': 'https://mdn.mozillademos.org/files/3075/svgdemo1.xml',
            'ad': 'http://www.orimi.com/pdf-test.pdf',
        }
        ```
        Note that line breaks are allowed in text, and employees must be given
        as int.
    """
    logo_missing = current_app.config['LOGO_MISSING']
    # ad_missing = current_app.config['AD_MISSING']  # TODO: Currently not used
    ad_ad = current_app.config['AD_AD']
    schema = Schema(And({
        # Top of page info
        "name": NONEMPY,
        "website": NONEMPY,
        "contact": NONEMPY,
        # Employees (at least one must be mentioned)
        Or("employees_ch", "employees_world"): Use(int),

        # Boxes on the side
        "booth": NONEMPY,
        "interested_in": And(["itet", "mavt", "mtec"], Use(_format_interests)),
        "offers": And(["bachelor", "master", "semester",
                       "fulltime", "internship", "trainee"],
                      Use(_format_offers)),

        # Description texts
        Optional("about"): NONEMPY,
        Optional("focus"): NONEMPY,

        # Logo and Ad (both must be urls to download files from)
        Optional('logo', default=logo_missing): Use(_get_logo),
        Optional('ad', default=ad_ad): Use(_get_ad),
    }, Use(_format_employees)))

    try:
        return schema.validate(data)
    except SchemaError as error:
        abort(make_response(jsonify(error=error.code), 422))


NONEMPY = And(str, len)


def _format_interests(values):
    """Make uppercase and sort to ensure consistency."""
    return sorted(item.upper() for item in values)


def _format_offers(data):
    """Sort and internationalize offers (currently german only)."""
    offers = {}
    thesis_offers = [thesis for thesis in ["bachelor", "master", "semester"]
                     if thesis in data]
    # If more then one item, put last on newline
    if len(thesis_offers) > 1:
        thesis_offers[-1] = "\n%s" % thesis_offers[-1]
    # Join them with kommas and write 'arbeiten' after the last one
    if thesis_offers:
        offers["thesis"] = "%sarbeiten" % ", ".join(thesis_offers)

    if "fulltime" in data:
        offers["fulltime"] = "Festanstellungen"

    entry_map = [("internship", "Praktika"), ("trainee", "Traineeprogramm")]
    entry_offers = [item for key, item in entry_map if key in data]
    if entry_offers:
        offers["entry"] = "\n".join(entry_offers)

    return offers


def _format_employees(data):
    """Combine swiss and worldwide employees into a single field."""
    def _format(key, suffix):
        """Re-format numbers and add suffix."""
        return "{:,} {}".format(data[key], suffix) if key in data else None

    swiss = _format('employees_ch', 'Schweiz\\minilangsep Switzerland')
    world = _format('employees_world', 'weltweit\\minilangsep worldwide')
    data['employees'] = r'\\ '.join(filter(None, [swiss, world]))
    return data


def _get_logo(url):
    """Download logo as `.svg`, and convert to `.pdf`."""
    with requests.get(url, stream=True) as response, \
            NamedTemporaryFile() as file:
        if response.status_code != 200:
            raise RuntimeError("File could not be downloaded.")

        for chunk in response:
            file.write(chunk)
        file.seek(0)

        # Dots in the wrong places can confuse latex includegraphics, remove all
        safe_name = secure_filename(url).replace(".", "")
        filepath = path.join(
            current_app.config["STORAGE_DIR"], "%s.pdf" % safe_name)
        makedirs(current_app.config["STORAGE_DIR"], exist_ok=True)

        # Convert to pdf
        arg_string = current_app.config['CONVERT_CMD'].format(
            input=file.name,
            output=filepath)
        subprocess.run(arg_string.split(), check=True)

    return filepath


def _get_ad(url):
    """Download ad as `.pdf`."""
    # Dots in the wrong places can confuse latex includegraphics, remove all
    safe_name = secure_filename(url).replace(".", "")
    filepath = path.join(
        current_app.config["STORAGE_DIR"], "%s.pdf" % safe_name)
    makedirs(current_app.config["STORAGE_DIR"], exist_ok=True)

    with requests.get(url, stream=True) as response, \
            open(filepath, 'wb') as file:
        if response.status_code != 200:
            raise RuntimeError("File could not be downloaded.")

        for chunk in response:
            file.write(chunk)

    return filepath
