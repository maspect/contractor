# -*- coding: utf-8 -*-

"""The app."""
import json
import subprocess
from datetime import datetime as dt
from functools import wraps
from io import BytesIO
from locale import LC_TIME, setlocale
from os import getcwd, getenv, path
from shutil import rmtree
from tempfile import NamedTemporaryFile
from uuid import uuid4
import hashlib

from flask import (Flask, abort, g, make_response, render_template, request,
                   send_file, url_for, redirect)
from jinja2 import PackageLoader, StrictUndefined
from jinjatex import Jinjatex
from raven.contrib.flask import Sentry
import requests
from werkzeug import secure_filename

from contractor.api import parse_company_data
from contractor.api_auth import logout as api_logout
from contractor.api_auth import protected
from contractor.fairguide_data import FAIRGUIDE_DATA
from contractor.soapclient import Importer

app = Flask('contractor')
app.config.from_pyfile('settings.py')

# Try to load config specified by envvar, default to config.py in work dir
default_config = path.join(getcwd(), 'config.py')
config_file = getenv("CONTRACTOR_CONFIG", default_config)
print("Attempting to load configuration:", config_file, flush=True)

app.config.from_pyfile(config_file)

# If directories have not been defined, use '.cache' in current working dir
app.config.setdefault('STORAGE_DIR', path.abspath('./.cache'))

# Set locale to ensure correct weekday format
app.config.setdefault('LOCALE', 'de_CH.utf-8')
setlocale(LC_TIME, app.config['LOCALE'])

try:
    sentry = Sentry(app, dsn=app.config['SENTRY_DSN'])
except KeyError:
    print("Sentry inactive: 'SENTRY_DSN' not found in config.", flush=True)

CRM = Importer(app.config['SOAP_USERNAME'], app.config['SOAP_PASSWORD'])

TEX = Jinjatex(tex_engine='xelatex',
               loader=PackageLoader('contractor', 'tex_templates'),
               undefined=StrictUndefined,
               trim_blocks=True)

TEX.env.filters.update({
    # Filters to parse date, including short one to list dates nicely
    # Format: Dienstag, 18.10.2016
    'fulldate': lambda date: dt.strftime(date, "%A, %d.%m.%Y"),
    # Format: Dienstag, 18.
    'shortdate': lambda date: dt.strftime(date, "%A, %d.")
})


def _file_response(data, filename, mimetype):
    """Make file response with appropriate headers to disable caching."""
    response = make_response(send_file(BytesIO(data),
                                       mimetype=mimetype,
                                       attachment_filename=filename,
                                       as_attachment=True,
                                       cache_timeout=0))
    response.headers['Content-Length'] = len(data)
    return response


def send(output_format, template, *args, **kwargs):
    """Send a file."""
    data = get_data(output_format, template, *args, **kwargs)
    name, mimetype = get_meta(output_format)
    return _file_response(data, name, mimetype)


def send_with_cache(cache_id, output_format, template, *args, **kwargs):
    """Send a file, preferrably from cache."""
    filepath = path.join(app.config['STORAGE_DIR'], str(cache_id))
    filename, mimetype = get_meta(output_format)

    # Check if data is cached
    try:
        with open(filepath, 'rb') as file:
            data = file.read()
    except FileNotFoundError:
        # Get data and cache for next time
        data = get_data(output_format, template, *args, **kwargs)
        with open(filepath, 'wb') as file:
            file.write(data)

    return _file_response(data, filename, mimetype)


def get_meta(output_format):
    """Return filename and mimetype based on requested format."""
    if output_format != 'tex':
        filename = '%s.pdf' % g.get('company', 'document')
        return (filename, 'application/pdf')
    else:
        filename = '%s.tex' % g.get('company', 'source')
        return (filename, 'text/plain')


def get_data(output_format, template, *args, **kwargs):
    """Prepare data, compress pdfs if needed."""
    if output_format != 'tex':
        data = TEX.compile_template(template, *args, **kwargs)

        # Compress the pdf if needed
        if output_format == 'compressed':
            with NamedTemporaryFile() as uncompressed:
                with NamedTemporaryFile() as compressed:
                    uncompressed.write(data)

                    arg_string = app.config['COMPRESS_CMD'].format(
                        input=uncompressed.name,
                        output=compressed.name)

                    subprocess.run(arg_string.split(), check=True)
                    data = compressed.read()

        return data
    else:
        data = TEX.render_template(template, *args, **kwargs)
        return data.encode()


def get_companies(company_id=None):
    """Get a selection of all companies, or just a single one."""
    if company_id is None:
        selection = CRM.get_companies()[0]  # select data of (data, errors)
    else:
        selection = [CRM.get_company(company_id)]
        g.company = secure_filename(selection[0]['name'])

    return selection


# Routes

@app.route('/', methods=['GET', 'POST'])
@protected
def main():
    """Main view.

    Includes output format and yearly settings.
    """
    (data, errors) = CRM.get_companies()

    # Companies with incomplete data for fair guide
    incomplete = [company for company in data
                  if not company['fairguide_ready']]

    return render_template('main.html',
                           user=g.get('username', ''),
                           yearly=app.config['YEARLY_SETTINGS'],
                           companies=data,
                           errors=errors,
                           warnings=incomplete)


@app.route('/clear', methods=['GET', 'POST'])
@protected
def clear_cache():
    """Clear cache, and redirect back to main."""
    # Remove dir and all contents, don't complain if directory is missing.
    rmtree(app.config['STORAGE_DIR'], ignore_errors=True)
    return redirect(url_for('main'))


@app.route('/logout')
def logout():
    """Log out."""
    return api_logout('You have been logged out. Goodbye!')


@app.route('/custom/', methods=['GET', 'POST'])
@protected
def custom():
    """View to show and create customizable letter."""
    fields = ['destination_address', 'subject', 'opening', 'body', 'closing',
              'signature', 'attachments']
    options = {field: request.form.get(field, '') for field in fields}

    # Fields must not be empty
    empty_ok = ['attachments']
    errors = {field: ((request.method == 'POST') and
                      (field not in empty_ok) and (not value))
              for field, value in options.items()}

    if request.method == 'POST' and not any(errors.values()):
        return send('.pdf', 'custom_letter.tex', **options)

    return render_template('custom.html',
                           user=g.username,
                           values=options,
                           errors=errors)


def download_endpoint(endpoint_name, protect=True):
    """Wrap the function as a flask endpoint.

    Accepts:
    <endpoint_name>/output_format/
    <endpoint_name>/output_format/company_id

    Companies are retrieved (without company id, all companies are fetched)
    and the function is called with:

    function(output_format, list_of_companies)
    """

    def _wrapper(func):
        @wraps(func)
        def _temp(output_format, company_id=None, **kwargs):
            companies = get_companies(company_id)
            if not companies:
                abort(404)

            return func(output_format, companies, **kwargs)

        _final = protected(_temp) if protect else _temp

        app.add_url_rule('/%s/<output_format>' % endpoint_name,
                         view_func=_final)
        app.add_url_rule('/%s/<output_format>/<company_id>' % endpoint_name,
                         view_func=_final)

        return _final
    return _wrapper


@download_endpoint('contracts')
def contracts(output_format, companies, german=True):
    """Contract creation."""
    # Check if output format is email -> only single contract
    contract_only = (output_format == "email")

    # Get yearly settings
    yearly = app.config['YEARLY_SETTINGS']
    letter_only = (output_format == "letter")

    options = dict(
        # Language
        german=german,

        # Data
        letterdata=companies,

        # Yearly settings
        fairtitle=yearly['fairtitle'],
        president=yearly['president'],
        sender=yearly['sender'],
        days=yearly['days'],
        prices=yearly['prices'],

        # Output options
        contract_only=contract_only,
        letter_only=letter_only
    )
    return send(output_format, 'contract.tex', **options)


@app.route('/contract_en/<output_format>/<company_id>')
@protected
def english_contract(output_format, company_id):
    """Contract creation in englisch. Should be moved to company setting."""
    return contracts(output_format, company_id, german=False)


# Unprotected, companies need to access it!
@app.route('/fairguide/<output_format>/<company_id>')
def companypage(output_format, company_id):
    """Return the rendered page for a single company."""
    companydata = get_companies(company_id)
    if not companydata:
        abort(404)

    return send(output_format, 'company_page.tex', companies=companydata)


@app.route('/fairguide/<output_format>')
@protected
def fairguide(output_format):
    """Create the fairguide."""
    companies = get_companies()
    cv_company = CRM.get_company(FAIRGUIDE_DATA['cv_check_company_id'])
    career_center = CRM.get_company(FAIRGUIDE_DATA['career_center_id'])

    return send(output_format,
                'fairguide.tex',
                print=(output_format == 'print'),
                companies=companies,
                cv_check_company=cv_company,
                career_center=career_center,
                **FAIRGUIDE_DATA)

# Unprotected, companies need to access it!
@app.route('/fairguidepage/<output_format>/<company_id>')
def fairguidepage(output_format, company_id):
    """Return the rendered page for a single company.

    The page is created based on company data proviced by the kontakt api
    and is cached.
    """
    response = requests.get(
        app.config['KONTAKTAPI_URL'] + 'previewdata/' + company_id)
    if response.status_code != 200:
        abort(404)

    data = response.json()
    cache_id = hashlib.sha256(
        json.dumps(data, sort_keys=True).encode('utf-8')).hexdigest()
    return send_with_cache(cache_id, output_format, 'company_page.tex',
                           companies=[parse_company_data(data)])


@download_endpoint('infoletter')
def infoletter(output_format, companies):
    """Create the information letter for the fair days."""
    return send(output_format,
                'infoletter.tex',
                companies=companies,
                fair_companies=get_companies(),
                **FAIRGUIDE_DATA)


@app.route('/boothplan/<output_format>')
@protected
def booth_plan(output_format):
    """Create the information letter for the fair days."""
    return send(output_format,
                'boothplan.tex',
                fair_companies=get_companies(),
                **FAIRGUIDE_DATA)


@download_endpoint('cvcontract')
def cv_contract(output_format, _):
    """Create the contract for the cv check company."""
    cv_company = CRM.get_company(FAIRGUIDE_DATA['cv_check_company_id'])

    return send(output_format,
                'cvcontract.tex',
                company=cv_company,
                **FAIRGUIDE_DATA)


@download_endpoint('feedback')
def feedback(output_format, companies):
    """Create the information letter for the fair days."""
    return send(output_format,
                'feedback.tex',
                companies=companies,
                title=FAIRGUIDE_DATA['title'])
